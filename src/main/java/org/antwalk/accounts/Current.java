package org.antwalk.accounts;

import org.antwalk.interfaces.Account;
import org.springframework.stereotype.Component;

@Component
public class Current implements Account {

	public void showAccout() {
		System.out.println("This is current account");

	}

}
