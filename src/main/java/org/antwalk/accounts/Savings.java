package org.antwalk.accounts;

import org.antwalk.interfaces.Account;
import org.springframework.stereotype.Component;


@Component
public class Savings implements Account {

	public void showAccout() {
		System.out.println("This is savings account");

	}
}
