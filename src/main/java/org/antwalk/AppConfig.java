package org.antwalk;

import org.antwalk.customers.Customer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

// where to scan to get all the components
@Configuration
@ComponentScan("org.antwalk")
public class AppConfig {
    @Bean("customer")
    public Customer cust(){
        return new Customer();
    }
	
}
