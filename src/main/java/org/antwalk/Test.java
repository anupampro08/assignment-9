package org.antwalk;

import java.util.Scanner;

import org.antwalk.customers.Customer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		
		Customer c1 = context.getBean("customer",Customer.class);
		
		String accType = null,name,uname,pass,address,ssn,email;
		int ch,age;
		long phone;
		float balance;
		Scanner in = new Scanner(System.in);
		
		System.out.println("-----Create Account -----");
		System.out.println("Select account type");
		System.out.println("1. Savings");
		System.out.println("2. Current");
		System.out.print("Enter choice: ");
		
		ch = in.nextInt();
		
		if(ch == 1)
			accType="savings";
		else if(ch==2) {
			accType="current";
		}
		else {
			System.out.print("Invalid Account");
		}
		c1.setAccount(context, accType);
		
		
		System.out.println("\n--Enter details--");
		System.out.println("Enter your name");
		name = in.next();
		c1.setName(name);
		
		System.out.println("Enter your username");
		uname = in.next();
		c1.setUsername(uname);
		
		System.out.println("Enter your password");
		pass = in.next();
		c1.setPassword(pass);
		
		System.out.println("Enter your age");
		age = in.nextInt();
		c1.setAge(12);
		
		System.out.println("Enter ssn");
		ssn = in.next();
		c1.setSsn(ssn);
		
		System.out.println("Enter your address");
		address = in.next();
		c1.setAddress(address);
		
		System.out.println("Enter your email");
		email = in.next();
		c1.setEmail(email);
		
		System.out.println("Enter your phone");
		phone = in.nextLong();
		c1.setPhone(phone);
		
		System.out.println("Enter initial balance");
		balance = in.nextFloat();
		c1.setBalance(balance);
		
		System.out.println("\nDisplaying Details");
		c1.displayDetails();

		
	}

}
