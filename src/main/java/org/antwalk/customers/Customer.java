package org.antwalk.customers;

import org.antwalk.interfaces.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

public class Customer {
	private String name;
	private String username;
	private String password;
	private int age;
	private String ssn;
	private String address;
	private String email;
	private long phone;
	private float balance;
	
	@Autowired
	@Qualifier("savings")
	Account account;

	public void setAccount(ApplicationContext context, String accountType) {
	        this.account = (Account) context.getBean(accountType);
	    }
	
	
	public Customer(String name, String username, String password, int age, String ssn, String address, String email,
			long phone, float balance)
	{
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.age = age;
		this.ssn = ssn;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.balance = balance;
	}

	public void displayDetails() {
		account.showAccout();
		System.out.println("Name: "+getName());
		System.out.println("Username: "+getUsername());
		System.out.println("Password: "+getPassword());
		System.out.println("Age: "+getAge());
		System.out.println("SSN: "+getSsn());
		System.out.println("Address: "+getAddress());
		System.out.println("Email: "+getEmail());
		System.out.println("Phone: "+getPhone());
		System.out.println("Balance: "+getBalance());
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	
}
